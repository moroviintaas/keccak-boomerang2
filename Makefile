IDIR=source/include
CXX=g++
CFLAGS=-I $(IDIR) -pipe -g -std=gnu++11 -Wall -W
DEBUG=-g

ODIR=obj
LDIR=../lib
SRC=source
LIBS=-lm

_DEPS = differential.h internalstate.h switchdiffs.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = differential.o internalstate.o main.o switchdiffs.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))
boomerang: $(OBJ)
	$(CXX) $(CFLAGS) -o $@ $^  $(LIBS) 

$(ODIR)/%.o: $(SRC)/%.cpp $(DEPS)
	$(CXX) -c $(CFLAGS) -o $@ $<  

all: boomerang
	printf "" 



.PHONY: clean delete makecopy purge
clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~
delete:
	rm cnfsat
mpurge: clean
	rm cnfsat
makecopy:
	cp Makefile backupMake
