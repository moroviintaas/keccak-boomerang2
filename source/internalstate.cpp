#include "include/internalstate.h"

#include <iostream> //do testów


 const unsigned long long InternalState::RC[] = {
     0x0000000000000001,
     0x0000000000008082,
     0x800000000000808a,
     0x8000000080008000,
     0x000000000000808b,
     0x0000000080000001,
     0x8000000080008081,
     0x8000000000008009,
     0x000000000000008a,
     0x0000000000000088,
     0x0000000080008009,
     0x000000008000000a,
     0x000000008000808b,
     0x800000000000008b,
     0x8000000000008089,
     0x8000000000008003,
     0x8000000000008002,
     0x8000000000000080,
     0x000000000000800a,
     0x800000008000000a,
     0x8000000080008081,
     0x8000000000008080,
     0x0000000080000001,
     0x8000000080008008,

 };


 const int InternalState::rho_offsets[][5] =
{
    {0,   36, 3, 41,  18 },
    {1,   44, 10,  45, 2 },
    {62,  6,  43, 15, 61 },
    {28,  55,  25, 21,56 },
    {27,  20, 39, 8, 14 }
};

InternalState::InternalState()
{
    for (int i=0; i<5; i++)
        for (int j=0; j<5; j++)
            lanes[i][j]=0;
}

InternalState InternalState::set_with_0()
{
    for (int i=0; i<5; i++)
        for (int j=0; j<5; j++)
            lanes[i][j]=0;
           // lanes[i][j]-=1;
    return *this;
}

InternalState InternalState::set_with_1()
{
    for (int i=0; i<5; i++)
        for (int j=0; j<5; j++)
            lanes[i][j]=-1;
           // lanes[i][j]-=1;
    return *this;
}

InternalState InternalState::set_with_rand()
{
    unsigned long long r;
    for (int i=0; i<5; i++)
        for (int j=0; j<5; j++)
        {
            r= rand();
            r<<=32;
            r^=rand();
            lanes[i][j]=r;
        }

    return *this;
}

InternalState InternalState::iterate()
{
    for(int x=0; x<5; x++)
        for(int y=0;y<5;y++)
        {
            lanes[x][y]++;
            if(lanes[x][y]!=0) return *this;
        }
    return *this;
}

InternalState InternalState::flat_iterate_l()
{
    unsigned long long bits;
    bits=0;
    int i=0;
    while( i<64 )
    {

        for(int x=0; x<5; x++)
        {
            for(int y=0; y<5; y++)
            {
                bits<<=1;
                //bits+=((lanes[x][y]&(1<<i))>>i);
                //bits<<=1;
                lanes[x][y]=ROR64(lanes[x][y],i);
                bits+=(lanes[x][y]&1);
                lanes[x][y]=ROL64(lanes[x][y],i);


            }
            //std::cout<<"bits:"<<std::hex<<bits<<"\n";
        }
        bits++;
        if(bits>=0x1ffffff)
        {
            bits=0;
            for(int x = 0;x<5;x++)
                for(int y=0; y<5; y++)
                    lanes[x][y]=0;
            i++;
        }
        else
        {
            for(int x=4; x>=0; x--)
            {
                for(int y=4; y>=0; y--)
                {
                    lanes[x][y]=ROR64(lanes[x][y],i);
                    lanes[x][y]^=(lanes[x][y]&1);
                    lanes[x][y]^=(bits&1);
                    lanes[x][y]=ROL64(lanes[x][y],i);

                    bits>>=1;
                }
            }
            return *this;
        }
    }
    return *this;
}

void InternalState::set_lane(int x, int y, unsigned long long l)
{
    lanes[x%5][y%5] = l;
}

void InternalState::xor_lane(int x, int y, unsigned long long l)
{
    lanes[x%5][y%5] ^= l;
}

void InternalState::set_m_significant_by_int_char(const InternalState &ch)
{

    unsigned long long l,c;
    for (unsigned int x = 0; x<5; x++)
    {
        for (unsigned int y=0; y<5; y++)
        {
            l= lanes[x][y];
            c = ch.lanes[x][y];
            l&= 0xffffffff;
            c&= 0xffffffff;
            l = ((l^c)<<32) ^l;
            lanes[x][y] = l;

        }
    }
}

InternalState InternalState::theta()
{
    unsigned long long C[5];
    unsigned long long D[5];

    for (int x=0 ; x<5; x++)
    {
        C[x] = lanes[x][0];
        for (int y=1; y<5; y++)
        {
            C[x] = C[x] ^ lanes[x][y];
        }
    }
    for (int x=0; x<5; ++x)
    {
        D[x] =  C[(x+4)%5] ^ ROL64(C[(x+1)%5], 1);
        for (int y=0; y<5; y++)
            lanes[x][y] ^= D[x];
    }
    return *this;

}

InternalState InternalState::pi()
{
    int X;
    int Y;
    InternalState clone = *this;
    for (int x=0; x<5; x++)
        for(int y=0; y<5; y++)
        {
            X = (0*x + 1*y)%5;
            Y = (2*x + 3*y)%5;
            lanes[X][Y] = clone.lanes[x][y];
        }
    return *this;


}

InternalState InternalState::rho()
{
    for(int x=0; x<5; x++)
        for(int y=0; y<5; y++)
            lanes[x][y] = ROL64(lanes[x][y],rho_offsets[x][y]);
    return *this;

}

InternalState InternalState::chi()
{
    unsigned long long D[5];
    for(int y=0; y<5; y++)
    {

        for(int x=0; x<5; x++)
            D[x] = lanes[x][y] ^ ((~lanes[(x+1)%5][y]) & lanes[(x+2)%5][y]);
        for (int x=0; x<5; x++)
            lanes[x][y] = D[x];

    }
    return *this;


}

InternalState InternalState::iota(int round_num)
{
    if(round_num<24)
        lanes[0][0]^= RC[round_num];
    return *this;

}

InternalState InternalState::ptheta()
{
    unsigned long long D[5];
    for (int x=0;x<5;x++)
    {
        D[x] = 0;
    }
    for(int y=0; y<5; y++)
    {
        D[0]^=lanes[4][y] ^ ROL64(lanes[1][y],1);
        D[1]^=lanes[0][y] ^ ROL64(lanes[2][y],1);
        D[2]^=lanes[1][y] ^ ROL64(lanes[3][y],1);
        D[3]^=lanes[2][y] ^ ROL64(lanes[4][y],1);
        D[4]^=lanes[3][y] ^ ROL64(lanes[0][y],1);
    }

    for(int x=0; x<5; x++)
    {
        for(int y=0; y<5; y++)
        {
            lanes[x][y]^=D[x];
        }
    }
    return *this;
}

InternalState InternalState::theta3()
{
    unsigned long long C[5];
        for(unsigned int x=0; x<5; x++) {
            C[x] = lanes[x][0];//[index(x,0)];
            for(unsigned int y=1; y<5; y++)
                C[x] ^= lanes[x][y];//A[index(x,y)];
        }
        unsigned long long D[5];
        for(unsigned int x=0; x<5; x++) {
            unsigned long long temp = C[(x+1)%5];
            temp = ROL64(temp, 1);
            D[x] = temp ^ C[(x+4)%5];
        }
        for(int x=0; x<5; x++)
            for(unsigned int y=0; y<5; y++)
                lanes[x][y] ^= D[x];
        return *this;
}

InternalState InternalState::lambda()
{
    theta();
    rho();
    pi();
    return *this;
}

InternalState InternalState::keccak_p(int start_on, int number_of_rounds)
{
    for(int i=start_on; i<start_on+number_of_rounds; i++)
    {
        theta();
        rho();
        pi();
        chi();
        iota(i);
    }
    return *this;


}

InternalState InternalState::reverse_theta()
{
    unsigned long long positions[5];
    positions[0] = 0xDE26BC4D789AF134;
    positions[1] = 0x09AF135E26BC4D78;
    positions[2] = 0xEBC4D789AF135E26;
    positions[3] = 0x7135E26BC4D789AF;
    positions[4] = 0xCD789AF135E26BC4;
    unsigned long long D[5];
    for(int x =0; x<5; x++)
    {
        D[x] = lanes[x][0];
        for(int y=1; y<5;y++)
        {
            D[x] ^= lanes[x][y];
        }
    }

    for( int z=0; z<64; z++)
    {
        for(int x_offset=0; x_offset<5; x_offset++)
        {
            for(int x=0; x<5; x++)
            {
                for(int y=0; y<5; y++)
                {
                    if((positions[x_offset] & 1) != 0)
                        lanes[x][y] ^= D[(x-x_offset+5)%5];
                }
            }
        }
        for(int x_offset=0; x_offset<5; x_offset++)
        {
            D[x_offset] = ROL64(D[x_offset],1);
            positions[x_offset]>>=1;
        }
    }
    return *this;


}
InternalState InternalState::inverse_theta()
{
    std::vector<UINT64> C(5);
    for(unsigned int x=0; x<5; x++) {
        C[x] = lanes[x][0];
        for(unsigned int y=1; y<5; y++){
            C[x] ^= lanes[x][y];
        }
    }
    const UINT64 inversePositions64[5] = {
        0xDE26BC4D789AF134ULL,
        0x09AF135E26BC4D78ULL,
        0xEBC4D789AF135E26ULL,
        0x7135E26BC4D789AFULL,
        0xCD789AF135E26BC4ULL };
    std::vector<UINT64> inversePositions(5, 0);
    //for(unsigned int z=0; z<64; z+=laneSize)
        for(unsigned int x=0; x<5; x++)
            inversePositions[x] ^= inversePositions64[x];
    for(unsigned int z=0; z<64; z++) {
        for(unsigned int xOff=0; xOff<5; xOff++)
           for(int x=0; x<5; x++)
               for(unsigned int y=0; y<5; y++)
                   if ((inversePositions[xOff] & 1) != 0)
                      lanes[x][y] ^= C[(x+5-xOff)%5];
        for(unsigned int xOff=0; xOff<5; xOff++) {
            C[xOff] = ROL64(C[xOff], 1);
            inversePositions[xOff] >>= 1;
        }
    }
}

/*
InternalState InternalState::preverse_theta()
{
    unsigned long long C[5], D[5];
    for (int x=0; x<5; x++)
    {
        C[x] = lanes[x][0];
        for(int y=1; y<5; y++)
        {
            C[x]^=lanes[x][y];
        }
    }
    D[0] = ROL64(C[0],2) ^ ROL64(C[0],4) ^ ROL64(C[0],5) ^ ROL64(C[0],8) ^ ROL64(C[0],12) ^ ROL64(C[0],13) ^ ROL64(C[0],14) ^ ROL64(C[0],15) ^ ROL64(C[0],17) ^ ROL64(C[0],19) ^ ROL64(C[0],20) ^ ROL64(C[0],23) ^ ROL64(C[0],27) ^ ROL64(C[0],28) ^ ROL64(C[0],29) ^ ROL64(C[0],30) ^ ROL64(C[0],32) ^ ROL64(C[0],34) ^ ROL64(C[0],35) ^ ROL64(C[0],38) ^ ROL64(C[0],42) ^ ROL64(C[0],43) ^ ROL64(C[0],44) ^ ROL64(C[0],45) ^ ROL64(C[0],47) ^ ROL64(C[0],49) ^ ROL64(C[0],50) ^ ROL64(C[0],53) ^ ROL64(C[0],57) ^ ROL64(C[0],58) ^ ROL64(C[0],59) ^ ROL64(C[0],60) ^ ROL64(C[0],62) ^ ROL64(C[0],63) ^
    ROL64(C[1],3) ^ ROL64(C[1],4) ^ ROL64(C[1],5) ^ ROL64(C[1],6) ^ ROL64(C[1],8) ^ ROL64(C[1],10) ^ ROL64(C[1],11) ^ ROL64(C[1],14) ^ ROL64(C[1],18) ^ ROL64(C[1],19) ^ ROL64(C[1],20) ^ ROL64(C[1],21) ^ ROL64(C[1],23) ^ ROL64(C[1],25) ^ ROL64(C[1],26) ^ ROL64(C[1],29) ^ ROL64(C[1],33) ^ ROL64(C[1],34) ^ ROL64(C[1],35) ^ ROL64(C[1],36) ^ ROL64(C[1],38) ^ ROL64(C[1],40) ^ ROL64(C[1],41) ^ ROL64(C[1],44) ^ ROL64(C[1],48) ^ ROL64(C[1],49) ^ ROL64(C[1],50) ^ ROL64(C[1],51) ^ ROL64(C[1],53) ^ ROL64(C[1],55) ^ ROL64(C[1],56) ^ ROL64(C[1],59) ^
    ROL64(C[2],1) ^ ROL64(C[2],2) ^ ROL64(C[2],5) ^ ROL64(C[2],9) ^ ROL64(C[2],10) ^ ROL64(C[2],11) ^ ROL64(C[2],12) ^ ROL64(C[2],14) ^ ROL64(C[2],16) ^ ROL64(C[2],17) ^ ROL64(C[2],20) ^ ROL64(C[2],24) ^ ROL64(C[2],25) ^ ROL64(C[2],26) ^ ROL64(C[2],27) ^ ROL64(C[2],29) ^ ROL64(C[2],31) ^ ROL64(C[2],32) ^ ROL64(C[2],35) ^ ROL64(C[2],39) ^ ROL64(C[2],40) ^ ROL64(C[2],41) ^ ROL64(C[2],42) ^ ROL64(C[2],44) ^ ROL64(C[2],46) ^ ROL64(C[2],47) ^ ROL64(C[2],50) ^ ROL64(C[2],54) ^ ROL64(C[2],55) ^ ROL64(C[2],56) ^ ROL64(C[2],57) ^ ROL64(C[2],59) ^ ROL64(C[2],61) ^ ROL64(C[2],62) ^ ROL64(C[2],63) ^
    C[3] ^ ROL64(C[3],1) ^ ROL64(C[3],2) ^ ROL64(C[3],3) ^ ROL64(C[3],5) ^ ROL64(C[3],7) ^ ROL64(C[3],8) ^ ROL64(C[3],11) ^ ROL64(C[3],15) ^ ROL64(C[3],16) ^ ROL64(C[3],17) ^ ROL64(C[3],18) ^ ROL64(C[3],20) ^ ROL64(C[3],22) ^ ROL64(C[3],23) ^ ROL64(C[3],26) ^ ROL64(C[3],30) ^ ROL64(C[3],31) ^ ROL64(C[3],32) ^ ROL64(C[3],33) ^ ROL64(C[3],35) ^ ROL64(C[3],37) ^ ROL64(C[3],38) ^ ROL64(C[3],41) ^ ROL64(C[3],45) ^ ROL64(C[3],46) ^ ROL64(C[3],47) ^ ROL64(C[3],48) ^ ROL64(C[3],50) ^ ROL64(C[3],52) ^ ROL64(C[3],53) ^ ROL64(C[3],56) ^ ROL64(C[3],60) ^ ROL64(C[3],61) ^ ROL64(C[3],62) ^
    ROL64(C[4],2) ^ ROL64(C[4],6) ^ ROL64(C[4],7) ^ ROL64(C[4],8) ^ ROL64(C[4],9) ^ ROL64(C[4],11) ^ ROL64(C[4],13) ^ ROL64(C[4],14) ^ ROL64(C[4],17) ^ ROL64(C[4],21) ^ ROL64(C[4],22) ^ ROL64(C[4],23) ^ ROL64(C[4],24) ^ ROL64(C[4],26) ^ ROL64(C[4],28) ^ ROL64(C[4],29) ^ ROL64(C[4],32) ^ ROL64(C[4],36) ^ ROL64(C[4],37) ^ ROL64(C[4],38) ^ ROL64(C[4],39) ^ ROL64(C[4],41) ^ ROL64(C[4],43) ^ ROL64(C[4],44) ^ ROL64(C[4],47) ^ ROL64(C[4],51) ^ ROL64(C[4],52) ^ ROL64(C[4],53) ^ ROL64(C[4],54) ^ ROL64(C[4],56) ^ ROL64(C[4],58) ^ ROL64(C[4],59) ^ ROL64(C[4],62) ^ ROL64(C[4],63);



    D[1] = ROL64(C[1],3) ^ ROL64(C[1],4) ^ ROL64(C[1],5) ^ ROL64(C[1],6) ^ ROL64(C[1],8) ^ ROL64(C[1],10) ^ ROL64(C[1],11) ^ ROL64(C[1],14) ^ ROL64(C[1],18) ^ ROL64(C[1],19) ^ ROL64(C[1],20) ^ ROL64(C[1],21) ^ ROL64(C[1],23) ^ ROL64(C[1],25) ^ ROL64(C[1],26) ^ ROL64(C[1],29) ^ ROL64(C[1],33) ^ ROL64(C[1],34) ^ ROL64(C[1],35) ^ ROL64(C[1],36) ^ ROL64(C[1],38) ^ ROL64(C[1],40) ^ ROL64(C[1],41) ^ ROL64(C[1],44) ^ ROL64(C[1],48) ^ ROL64(C[1],49) ^ ROL64(C[1],50) ^ ROL64(C[1],51) ^ ROL64(C[1],53) ^ ROL64(C[1],55) ^ ROL64(C[1],56) ^ ROL64(C[1],59) ^
    ROL64(C[2],1) ^ ROL64(C[2],2) ^ ROL64(C[2],5) ^ ROL64(C[2],9) ^ ROL64(C[2],10) ^ ROL64(C[2],11) ^ ROL64(C[2],12) ^ ROL64(C[2],14) ^ ROL64(C[2],16) ^ ROL64(C[2],17) ^ ROL64(C[2],20) ^ ROL64(C[2],24) ^ ROL64(C[2],25) ^ ROL64(C[2],26) ^ ROL64(C[2],27) ^ ROL64(C[2],29) ^ ROL64(C[2],31) ^ ROL64(C[2],32) ^ ROL64(C[2],35) ^ ROL64(C[2],39) ^ ROL64(C[2],40) ^ ROL64(C[2],41) ^ ROL64(C[2],42) ^ ROL64(C[2],44) ^ ROL64(C[2],46) ^ ROL64(C[2],47) ^ ROL64(C[2],50) ^ ROL64(C[2],54) ^ ROL64(C[2],55) ^ ROL64(C[2],56) ^ ROL64(C[2],57) ^ ROL64(C[2],59) ^ ROL64(C[2],61) ^ ROL64(C[2],62) ^ ROL64(C[2],63) ^
    C[3] ^ ROL64(C[3],1) ^ ROL64(C[3],2) ^ ROL64(C[3],3) ^ ROL64(C[3],5) ^ ROL64(C[3],7) ^ ROL64(C[3],8) ^ ROL64(C[3],11) ^ ROL64(C[3],15) ^ ROL64(C[3],16) ^ ROL64(C[3],17) ^ ROL64(C[3],18) ^ ROL64(C[3],20) ^ ROL64(C[3],22) ^ ROL64(C[3],23) ^ ROL64(C[3],26) ^ ROL64(C[3],30) ^ ROL64(C[3],31) ^ ROL64(C[3],32) ^ ROL64(C[3],33) ^ ROL64(C[3],35) ^ ROL64(C[3],37) ^ ROL64(C[3],38) ^ ROL64(C[3],41) ^ ROL64(C[3],45) ^ ROL64(C[3],46) ^ ROL64(C[3],47) ^ ROL64(C[3],48) ^ ROL64(C[3],50) ^ ROL64(C[3],52) ^ ROL64(C[3],53) ^ ROL64(C[3],56) ^ ROL64(C[3],60) ^ ROL64(C[3],61) ^ ROL64(C[3],62) ^
    ROL64(C[4],2) ^ ROL64(C[4],6) ^ ROL64(C[4],7) ^ ROL64(C[4],8) ^ ROL64(C[4],9) ^ ROL64(C[4],11) ^ ROL64(C[4],13) ^ ROL64(C[4],14) ^ ROL64(C[4],17) ^ ROL64(C[4],21) ^ ROL64(C[4],22) ^ ROL64(C[4],23) ^ ROL64(C[4],24) ^ ROL64(C[4],26) ^ ROL64(C[4],28) ^ ROL64(C[4],29) ^ ROL64(C[4],32) ^ ROL64(C[4],36) ^ ROL64(C[4],37) ^ ROL64(C[4],38) ^ ROL64(C[4],39) ^ ROL64(C[4],41) ^ ROL64(C[4],43) ^ ROL64(C[4],44) ^ ROL64(C[4],47) ^ ROL64(C[4],51) ^ ROL64(C[4],52) ^ ROL64(C[4],53) ^ ROL64(C[4],54) ^ ROL64(C[4],56) ^ ROL64(C[4],58) ^ ROL64(C[4],59) ^ ROL64(C[4],62) ^ ROL64(C[4],63) ^
    ROL64(C[0],2) ^ ROL64(C[0],4) ^ ROL64(C[0],5) ^ ROL64(C[0],8) ^ ROL64(C[0],12) ^ ROL64(C[0],13) ^ ROL64(C[0],14) ^ ROL64(C[0],15) ^ ROL64(C[0],17) ^ ROL64(C[0],19) ^ ROL64(C[0],20) ^ ROL64(C[0],23) ^ ROL64(C[0],27) ^ ROL64(C[0],28) ^ ROL64(C[0],29) ^ ROL64(C[0],30) ^ ROL64(C[0],32) ^ ROL64(C[0],34) ^ ROL64(C[0],35) ^ ROL64(C[0],38) ^ ROL64(C[0],42) ^ ROL64(C[0],43) ^ ROL64(C[0],44) ^ ROL64(C[0],45) ^ ROL64(C[0],47) ^ ROL64(C[0],49) ^ ROL64(C[0],50) ^ ROL64(C[0],53) ^ ROL64(C[0],57) ^ ROL64(C[0],58) ^ ROL64(C[0],59) ^ ROL64(C[0],60) ^ ROL64(C[0],62) ^ ROL64(C[0],63);



    D[2] = ROL64(C[2],1) ^ ROL64(C[2],2) ^ ROL64(C[2],5) ^ ROL64(C[2],9) ^ ROL64(C[2],10) ^ ROL64(C[2],11) ^ ROL64(C[2],12) ^ ROL64(C[2],14) ^ ROL64(C[2],16) ^ ROL64(C[2],17) ^ ROL64(C[2],20) ^ ROL64(C[2],24) ^ ROL64(C[2],25) ^ ROL64(C[2],26) ^ ROL64(C[2],27) ^ ROL64(C[2],29) ^ ROL64(C[2],31) ^ ROL64(C[2],32) ^ ROL64(C[2],35) ^ ROL64(C[2],39) ^ ROL64(C[2],40) ^ ROL64(C[2],41) ^ ROL64(C[2],42) ^ ROL64(C[2],44) ^ ROL64(C[2],46) ^ ROL64(C[2],47) ^ ROL64(C[2],50) ^ ROL64(C[2],54) ^ ROL64(C[2],55) ^ ROL64(C[2],56) ^ ROL64(C[2],57) ^ ROL64(C[2],59) ^ ROL64(C[2],61) ^ ROL64(C[2],62) ^ ROL64(C[2],63) ^
    C[3] ^ ROL64(C[3],1) ^ ROL64(C[3],2) ^ ROL64(C[3],3) ^ ROL64(C[3],5) ^ ROL64(C[3],7) ^ ROL64(C[3],8) ^ ROL64(C[3],11) ^ ROL64(C[3],15) ^ ROL64(C[3],16) ^ ROL64(C[3],17) ^ ROL64(C[3],18) ^ ROL64(C[3],20) ^ ROL64(C[3],22) ^ ROL64(C[3],23) ^ ROL64(C[3],26) ^ ROL64(C[3],30) ^ ROL64(C[3],31) ^ ROL64(C[3],32) ^ ROL64(C[3],33) ^ ROL64(C[3],35) ^ ROL64(C[3],37) ^ ROL64(C[3],38) ^ ROL64(C[3],41) ^ ROL64(C[3],45) ^ ROL64(C[3],46) ^ ROL64(C[3],47) ^ ROL64(C[3],48) ^ ROL64(C[3],50) ^ ROL64(C[3],52) ^ ROL64(C[3],53) ^ ROL64(C[3],56) ^ ROL64(C[3],60) ^ ROL64(C[3],61) ^ ROL64(C[3],62) ^
    ROL64(C[4],2) ^ ROL64(C[4],6) ^ ROL64(C[4],7) ^ ROL64(C[4],8) ^ ROL64(C[4],9) ^ ROL64(C[4],11) ^ ROL64(C[4],13) ^ ROL64(C[4],14) ^ ROL64(C[4],17) ^ ROL64(C[4],21) ^ ROL64(C[4],22) ^ ROL64(C[4],23) ^ ROL64(C[4],24) ^ ROL64(C[4],26) ^ ROL64(C[4],28) ^ ROL64(C[4],29) ^ ROL64(C[4],32) ^ ROL64(C[4],36) ^ ROL64(C[4],37) ^ ROL64(C[4],38) ^ ROL64(C[4],39) ^ ROL64(C[4],41) ^ ROL64(C[4],43) ^ ROL64(C[4],44) ^ ROL64(C[4],47) ^ ROL64(C[4],51) ^ ROL64(C[4],52) ^ ROL64(C[4],53) ^ ROL64(C[4],54) ^ ROL64(C[4],56) ^ ROL64(C[4],58) ^ ROL64(C[4],59) ^ ROL64(C[4],62) ^ ROL64(C[4],63) ^
    ROL64(C[0],2) ^ ROL64(C[0],4) ^ ROL64(C[0],5) ^ ROL64(C[0],8) ^ ROL64(C[0],12) ^ ROL64(C[0],13) ^ ROL64(C[0],14) ^ ROL64(C[0],15) ^ ROL64(C[0],17) ^ ROL64(C[0],19) ^ ROL64(C[0],20) ^ ROL64(C[0],23) ^ ROL64(C[0],27) ^ ROL64(C[0],28) ^ ROL64(C[0],29) ^ ROL64(C[0],30) ^ ROL64(C[0],32) ^ ROL64(C[0],34) ^ ROL64(C[0],35) ^ ROL64(C[0],38) ^ ROL64(C[0],42) ^ ROL64(C[0],43) ^ ROL64(C[0],44) ^ ROL64(C[0],45) ^ ROL64(C[0],47) ^ ROL64(C[0],49) ^ ROL64(C[0],50) ^ ROL64(C[0],53) ^ ROL64(C[0],57) ^ ROL64(C[0],58) ^ ROL64(C[0],59) ^ ROL64(C[0],60) ^ ROL64(C[0],62) ^ ROL64(C[0],63) ^
    ROL64(C[1],3) ^ ROL64(C[1],4) ^ ROL64(C[1],5) ^ ROL64(C[1],6) ^ ROL64(C[1],8) ^ ROL64(C[1],10) ^ ROL64(C[1],11) ^ ROL64(C[1],14) ^ ROL64(C[1],18) ^ ROL64(C[1],19) ^ ROL64(C[1],20) ^ ROL64(C[1],21) ^ ROL64(C[1],23) ^ ROL64(C[1],25) ^ ROL64(C[1],26) ^ ROL64(C[1],29) ^ ROL64(C[1],33) ^ ROL64(C[1],34) ^ ROL64(C[1],35) ^ ROL64(C[1],36) ^ ROL64(C[1],38) ^ ROL64(C[1],40) ^ ROL64(C[1],41) ^ ROL64(C[1],44) ^ ROL64(C[1],48) ^ ROL64(C[1],49) ^ ROL64(C[1],50) ^ ROL64(C[1],51) ^ ROL64(C[1],53) ^ ROL64(C[1],55) ^ ROL64(C[1],56) ^ ROL64(C[1],59);



    D[3] = C[3] ^ ROL64(C[3],1) ^ ROL64(C[3],2) ^ ROL64(C[3],3) ^ ROL64(C[3],5) ^ ROL64(C[3],7) ^ ROL64(C[3],8) ^ ROL64(C[3],11) ^ ROL64(C[3],15) ^ ROL64(C[3],16) ^ ROL64(C[3],17) ^ ROL64(C[3],18) ^ ROL64(C[3],20) ^ ROL64(C[3],22) ^ ROL64(C[3],23) ^ ROL64(C[3],26) ^ ROL64(C[3],30) ^ ROL64(C[3],31) ^ ROL64(C[3],32) ^ ROL64(C[3],33) ^ ROL64(C[3],35) ^ ROL64(C[3],37) ^ ROL64(C[3],38) ^ ROL64(C[3],41) ^ ROL64(C[3],45) ^ ROL64(C[3],46) ^ ROL64(C[3],47) ^ ROL64(C[3],48) ^ ROL64(C[3],50) ^ ROL64(C[3],52) ^ ROL64(C[3],53) ^ ROL64(C[3],56) ^ ROL64(C[3],60) ^ ROL64(C[3],61) ^ ROL64(C[3],62) ^
    ROL64(C[4],2) ^ ROL64(C[4],6) ^ ROL64(C[4],7) ^ ROL64(C[4],8) ^ ROL64(C[4],9) ^ ROL64(C[4],11) ^ ROL64(C[4],13) ^ ROL64(C[4],14) ^ ROL64(C[4],17) ^ ROL64(C[4],21) ^ ROL64(C[4],22) ^ ROL64(C[4],23) ^ ROL64(C[4],24) ^ ROL64(C[4],26) ^ ROL64(C[4],28) ^ ROL64(C[4],29) ^ ROL64(C[4],32) ^ ROL64(C[4],36) ^ ROL64(C[4],37) ^ ROL64(C[4],38) ^ ROL64(C[4],39) ^ ROL64(C[4],41) ^ ROL64(C[4],43) ^ ROL64(C[4],44) ^ ROL64(C[4],47) ^ ROL64(C[4],51) ^ ROL64(C[4],52) ^ ROL64(C[4],53) ^ ROL64(C[4],54) ^ ROL64(C[4],56) ^ ROL64(C[4],58) ^ ROL64(C[4],59) ^ ROL64(C[4],62) ^ ROL64(C[4],63) ^
    ROL64(C[0],2) ^ ROL64(C[0],4) ^ ROL64(C[0],5) ^ ROL64(C[0],8) ^ ROL64(C[0],12) ^ ROL64(C[0],13) ^ ROL64(C[0],14) ^ ROL64(C[0],15) ^ ROL64(C[0],17) ^ ROL64(C[0],19) ^ ROL64(C[0],20) ^ ROL64(C[0],23) ^ ROL64(C[0],27) ^ ROL64(C[0],28) ^ ROL64(C[0],29) ^ ROL64(C[0],30) ^ ROL64(C[0],32) ^ ROL64(C[0],34) ^ ROL64(C[0],35) ^ ROL64(C[0],38) ^ ROL64(C[0],42) ^ ROL64(C[0],43) ^ ROL64(C[0],44) ^ ROL64(C[0],45) ^ ROL64(C[0],47) ^ ROL64(C[0],49) ^ ROL64(C[0],50) ^ ROL64(C[0],53) ^ ROL64(C[0],57) ^ ROL64(C[0],58) ^ ROL64(C[0],59) ^ ROL64(C[0],60) ^ ROL64(C[0],62) ^ ROL64(C[0],63) ^
    ROL64(C[1],3) ^ ROL64(C[1],4) ^ ROL64(C[1],5) ^ ROL64(C[1],6) ^ ROL64(C[1],8) ^ ROL64(C[1],10) ^ ROL64(C[1],11) ^ ROL64(C[1],14) ^ ROL64(C[1],18) ^ ROL64(C[1],19) ^ ROL64(C[1],20) ^ ROL64(C[1],21) ^ ROL64(C[1],23) ^ ROL64(C[1],25) ^ ROL64(C[1],26) ^ ROL64(C[1],29) ^ ROL64(C[1],33) ^ ROL64(C[1],34) ^ ROL64(C[1],35) ^ ROL64(C[1],36) ^ ROL64(C[1],38) ^ ROL64(C[1],40) ^ ROL64(C[1],41) ^ ROL64(C[1],44) ^ ROL64(C[1],48) ^ ROL64(C[1],49) ^ ROL64(C[1],50) ^ ROL64(C[1],51) ^ ROL64(C[1],53) ^ ROL64(C[1],55) ^ ROL64(C[1],56) ^ ROL64(C[1],59) ^
    ROL64(C[2],1) ^ ROL64(C[2],2) ^ ROL64(C[2],5) ^ ROL64(C[2],9) ^ ROL64(C[2],10) ^ ROL64(C[2],11) ^ ROL64(C[2],12) ^ ROL64(C[2],14) ^ ROL64(C[2],16) ^ ROL64(C[2],17) ^ ROL64(C[2],20) ^ ROL64(C[2],24) ^ ROL64(C[2],25) ^ ROL64(C[2],26) ^ ROL64(C[2],27) ^ ROL64(C[2],29) ^ ROL64(C[2],31) ^ ROL64(C[2],32) ^ ROL64(C[2],35) ^ ROL64(C[2],39) ^ ROL64(C[2],40) ^ ROL64(C[2],41) ^ ROL64(C[2],42) ^ ROL64(C[2],44) ^ ROL64(C[2],46) ^ ROL64(C[2],47) ^ ROL64(C[2],50) ^ ROL64(C[2],54) ^ ROL64(C[2],55) ^ ROL64(C[2],56) ^ ROL64(C[2],57) ^ ROL64(C[2],59) ^ ROL64(C[2],61) ^ ROL64(C[2],62) ^ ROL64(C[2],63);



    D[4] = ROL64(C[4],2) ^ ROL64(C[4],6) ^ ROL64(C[4],7) ^ ROL64(C[4],8) ^ ROL64(C[4],9) ^ ROL64(C[4],11) ^ ROL64(C[4],13) ^ ROL64(C[4],14) ^ ROL64(C[4],17) ^ ROL64(C[4],21) ^ ROL64(C[4],22) ^ ROL64(C[4],23) ^ ROL64(C[4],24) ^ ROL64(C[4],26) ^ ROL64(C[4],28) ^ ROL64(C[4],29) ^ ROL64(C[4],32) ^ ROL64(C[4],36) ^ ROL64(C[4],37) ^ ROL64(C[4],38) ^ ROL64(C[4],39) ^ ROL64(C[4],41) ^ ROL64(C[4],43) ^ ROL64(C[4],44) ^ ROL64(C[4],47) ^ ROL64(C[4],51) ^ ROL64(C[4],52) ^ ROL64(C[4],53) ^ ROL64(C[4],54) ^ ROL64(C[4],56) ^ ROL64(C[4],58) ^ ROL64(C[4],59) ^ ROL64(C[4],62) ^ ROL64(C[4],63) ^
    ROL64(C[0],2) ^ ROL64(C[0],4) ^ ROL64(C[0],5) ^ ROL64(C[0],8) ^ ROL64(C[0],12) ^ ROL64(C[0],13) ^ ROL64(C[0],14) ^ ROL64(C[0],15) ^ ROL64(C[0],17) ^ ROL64(C[0],19) ^ ROL64(C[0],20) ^ ROL64(C[0],23) ^ ROL64(C[0],27) ^ ROL64(C[0],28) ^ ROL64(C[0],29) ^ ROL64(C[0],30) ^ ROL64(C[0],32) ^ ROL64(C[0],34) ^ ROL64(C[0],35) ^ ROL64(C[0],38) ^ ROL64(C[0],42) ^ ROL64(C[0],43) ^ ROL64(C[0],44) ^ ROL64(C[0],45) ^ ROL64(C[0],47) ^ ROL64(C[0],49) ^ ROL64(C[0],50) ^ ROL64(C[0],53) ^ ROL64(C[0],57) ^ ROL64(C[0],58) ^ ROL64(C[0],59) ^ ROL64(C[0],60) ^ ROL64(C[0],62) ^ ROL64(C[0],63) ^
    ROL64(C[1],3) ^ ROL64(C[1],4) ^ ROL64(C[1],5) ^ ROL64(C[1],6) ^ ROL64(C[1],8) ^ ROL64(C[1],10) ^ ROL64(C[1],11) ^ ROL64(C[1],14) ^ ROL64(C[1],18) ^ ROL64(C[1],19) ^ ROL64(C[1],20) ^ ROL64(C[1],21) ^ ROL64(C[1],23) ^ ROL64(C[1],25) ^ ROL64(C[1],26) ^ ROL64(C[1],29) ^ ROL64(C[1],33) ^ ROL64(C[1],34) ^ ROL64(C[1],35) ^ ROL64(C[1],36) ^ ROL64(C[1],38) ^ ROL64(C[1],40) ^ ROL64(C[1],41) ^ ROL64(C[1],44) ^ ROL64(C[1],48) ^ ROL64(C[1],49) ^ ROL64(C[1],50) ^ ROL64(C[1],51) ^ ROL64(C[1],53) ^ ROL64(C[1],55) ^ ROL64(C[1],56) ^ ROL64(C[1],59) ^
    ROL64(C[2],1) ^ ROL64(C[2],2) ^ ROL64(C[2],5) ^ ROL64(C[2],9) ^ ROL64(C[2],10) ^ ROL64(C[2],11) ^ ROL64(C[2],12) ^ ROL64(C[2],14) ^ ROL64(C[2],16) ^ ROL64(C[2],17) ^ ROL64(C[2],20) ^ ROL64(C[2],24) ^ ROL64(C[2],25) ^ ROL64(C[2],26) ^ ROL64(C[2],27) ^ ROL64(C[2],29) ^ ROL64(C[2],31) ^ ROL64(C[2],32) ^ ROL64(C[2],35) ^ ROL64(C[2],39) ^ ROL64(C[2],40) ^ ROL64(C[2],41) ^ ROL64(C[2],42) ^ ROL64(C[2],44) ^ ROL64(C[2],46) ^ ROL64(C[2],47) ^ ROL64(C[2],50) ^ ROL64(C[2],54) ^ ROL64(C[2],55) ^ ROL64(C[2],56) ^ ROL64(C[2],57) ^ ROL64(C[2],59) ^ ROL64(C[2],61) ^ ROL64(C[2],62) ^ ROL64(C[2],63) ^
    C[3] ^ ROL64(C[3],1) ^ ROL64(C[3],2) ^ ROL64(C[3],3) ^ ROL64(C[3],5) ^ ROL64(C[3],7) ^ ROL64(C[3],8) ^ ROL64(C[3],11) ^ ROL64(C[3],15) ^ ROL64(C[3],16) ^ ROL64(C[3],17) ^ ROL64(C[3],18) ^ ROL64(C[3],20) ^ ROL64(C[3],22) ^ ROL64(C[3],23) ^ ROL64(C[3],26) ^ ROL64(C[3],30) ^ ROL64(C[3],31) ^ ROL64(C[3],32) ^ ROL64(C[3],33) ^ ROL64(C[3],35) ^ ROL64(C[3],37) ^ ROL64(C[3],38) ^ ROL64(C[3],41) ^ ROL64(C[3],45) ^ ROL64(C[3],46) ^ ROL64(C[3],47) ^ ROL64(C[3],48) ^ ROL64(C[3],50) ^ ROL64(C[3],52) ^ ROL64(C[3],53) ^ ROL64(C[3],56) ^ ROL64(C[3],60) ^ ROL64(C[3],61) ^ ROL64(C[3],62);


    for(int x=0; x<5; x++)
        for(int y=0; y<5;y++)
            lanes[x][y]^=D[x];
    return *this;
}*/

InternalState InternalState::reverse_pi()
{
    int X;
    int Y;
    InternalState clone = *this;
    for (int x=0; x<5; x++)
        for(int y=0; y<5; y++)
        {
            X = (1*x + 3*y)%5;
            Y = (1*x + 0*y)%5;
            lanes[X][Y] = clone.lanes[x][y];
        }

    return *this;
}

InternalState InternalState::reverse_rho()
{
    for(int x=0; x<5; x++)
        for(int y=0; y<5; y++)
            lanes[x][y] = ROL64(lanes[x][y],(64-rho_offsets[x][y]));
    return *this;
}

InternalState InternalState::reverse_chi()
{
    unsigned long long D[5];
    int X_;
    for(int y=0; y<5; y++)
    {
        for (int x=0; x<5; x++)
        {
            D[x] = lanes[x][y];
        }
        for(int x=0; x<6; x++)
        {
            X_ = (3*x)%5;
            lanes[X_][y] = D[X_] ^ (lanes[(X_+2)%5][y] & (~D[(X_+1)%5]));
        }
    }
    return *this;
}

InternalState InternalState::reverse_iota(int round_num)
{
    iota(round_num);
    return *this;
}

InternalState InternalState::reverse_lambda()
{
    reverse_pi();
    reverse_rho();
    reverse_theta();
    return *this;
}

InternalState InternalState::reverse_keccak_p(int start_on, int number_of_rounds)
{
    for(int i=start_on; i>start_on-number_of_rounds; i--)
    {
        reverse_iota(i);
        reverse_chi();
        reverse_pi();
        reverse_rho();
        reverse_theta();
    }
    return *this;
}

unsigned int InternalState::get_row(int y, int z) const
{
    y%=5;
    z%=64;
    int result = 0;
    for(int x=5; x>=0; x--)
    {
        result<<=1;
        result^=(lanes[x][y]>>z)&1;

    }
    return result & 0x1f;
}

void InternalState::set_row(int row, int y, int z)
{
    y%=5;
    z%=64;
    for(int x=0; x<5; x++)
    {
        lanes[x][y]=ROR64(lanes[x][y],z);
        lanes[x][y]>>=1;
        lanes[x][y]<<=1;
        lanes[x][y]^=(row&1);
        lanes[x][y]=ROL64(lanes[x][y],z);
        row>>=1;
    }
}

unsigned int InternalState::get_bit(int x, int y, int z) const
{
    unsigned long long l= lanes[x][y];
    return (unsigned int) ((l>>z) & 1);
}

void InternalState::set_bit(int x, int y, int z, int v)
{
    unsigned long long l = lanes[x][y];
    l = ROR64(l,z);
    l >>=1;
    l <<=1;
    unsigned long long nv = v&1;
    l ^=nv;
    l = ROL64(l,z);
    lanes[x][y]=l;
}

InternalState InternalState::operator^(const InternalState &s2) const
{
    InternalState result;
    for (int x=0; x<5; x++)
        for(int y=0; y<5; y++)
            result.lanes[x][y] = this->lanes[x][y]^s2.lanes[x][y];
    return result;
}

InternalState InternalState::operator&(const InternalState &s2) const
{
    InternalState result;
    for (int x=0; x<5; x++)
        for(int y=0; y<5; y++)
            result.lanes[x][y] = this->lanes[x][y]&s2.lanes[x][y];
    return result;
}

InternalState InternalState::operator|(const InternalState &s2) const
{
    InternalState result;
    for (int x=0; x<5; x++)
        for(int y=0; y<5; y++)
            result.lanes[x][y] = (this->lanes[x][y]|s2.lanes[x][y]);
    return result;
}



InternalState InternalState::int_diff() const
{
    InternalState result;
    for(int i=0;i<5; i++)
    {
        for (int j=0; j<5; j++)
        {
            result.lanes[i][j] = 0xffffffff & (this->lanes[i][j] ^ (this->lanes[i][j]>>32));

        }
    }
    return result;
}

InternalState InternalState::diff(const InternalState &s2) const
{
    InternalState result;
    for(int i=0;i<5; i++)
    {
        for (int j=0; j<5; j++)
        {
            result.lanes[i][j] = (this->lanes[i][j] ^ s2.lanes[i][j]);

        }
    }
    return result;
}

bool InternalState::operator==(const InternalState &s2) const
{
    for (int x=0 ; x<5; x++)
        for (int y=0; y<5; y++)
            if(this->lanes[x][y] != s2.lanes[x][y]) return false;
    return true;
}

bool InternalState::operator !=(const InternalState &s2) const
{
    return !((*this)==s2);
}

InternalState &InternalState::operator =(const InternalState s2)
{
    for (int x=0; x<5; x++)
        for(int y=0; y<5; y++)
            this->lanes[x][y] = s2.lanes[x][y];
    return *this;
}

bool InternalState::operator<(const InternalState & other) const
{
    for(size_t x= 0; x<5; x++)
    {
        for(size_t y=0; y<5; y++)
        {
            if (lanes[x][y]< other.lanes[x][y])
                    return true;
        }


    }
    return false;
}

std::ostream &InternalState::stream_out_half(std::ostream &os)
{
    os <<STATE_BEG_HEADER<<"\n";
    for (int y=0; y<5; y++)
    {
        for (int x=0; x<5; x++)
        {
            os<<std::hex<<std::setw(8)<<std::setfill('0')<<lanes[x][y]<<" ";
        }
        os<<"\n";
    }
    os <<STATE_END_HEADER<<"\n";
    return os;

}

void InternalState::tst()
{
    for(int i=0; i<24; i++)
        std::cout<<RC[i]<<"\n";

}

void InternalState::tst_rhoc()
{
    int tab[5][5];
    tab[0][0] = 0;
    unsigned int x = 1;
    unsigned int y = 0;
    for(unsigned int t=0; t<24; t++) {
        tab[x][y] = ((t+1)*(t+2)/2) %64;
        unsigned int newX = (0*x + 1*y)%5;
        unsigned int newY = (2*x + 3*y)%5;
        x = newX;
        y = newY;
        }

    for (int x =0; x<5;x++)
        for(int y=0; y<5; y++)
        {
            std::cout<<x<<" "<<y<<" "<<std::hex<<std::setw(2)<<rho_offsets[x][y]<<" ";
            std::cout<<std::hex<<std::setw(2)<<tab[x][y]<<" ";
            std::cout<<((rho_offsets[x][y]==tab[x][y])?"ok":"##")<<"\n";
        }

}





std::istream & operator>>(std::istream &is, InternalState &state)
{
    std::string buf;
    do
    {
        is>>buf;
    }while(buf != STATE_BEG_HEADER);
    for(int y=0; y<5; y++)
    {
        for (int x=0; x<5; x++)
        {
            is>>std::hex>>state.lanes[x][y];
        }
    }
    return is;
}

std::ostream & operator<<(std::ostream &os, const InternalState &state)
{
    os <<STATE_BEG_HEADER<<"\n";
    for (int y=0; y<5; y++)
    {
        for (int x=0; x<5; x++)
        {
            os<<std::hex<<std::setw(16)<<std::setfill('0')<<state.lanes[x][y]<<" ";
        }
        os<<"\n";
    }
    os <<STATE_END_HEADER<<"\n";
    return os;

}


