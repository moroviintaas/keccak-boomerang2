#ifndef MATCHCANDIDATE_H
#define MATCHCANDIDATE_H

#include "internalstate.h"
#include "differential.h"
class SwitchDiffs
{
private:
    InternalState Left;
    InternalState Right;
    InternalState R_before_chi_std;
    InternalState L_before_chi_std;
    InternalState R_after_chi_int;
    InternalState L_after_chi_int;
public:
    SwitchDiffs();
    SwitchDiffs(const InternalState &L, const Differential &diff, int iota_num);

    InternalState getLeft() const;
    InternalState getRight() const;
    InternalState getR_before_chi_std() const;
    InternalState getL_before_chi_std() const;
    InternalState getL_after_chi_int() const;
    InternalState getR_after_chi_int() const;


    void set(const InternalState &Left, const Differential &diff, int iota_num);
};

#endif // MATCHCANDIDATE_H
