#ifndef INTERNALSTATE_H
#define INTERNALSTATE_H
#include <vector>
//#include "halfstate.h"
#include <iostream>
#include <iomanip>
#include <string>
#define STATE_BEG_HEADER "=====Begin_State====="
#define STATE_END_HEADER "======End_State======"
#define HALFSTATE_BEG_HEADER "====Begin_HalfState===="
#define HALFSTATE_END_HEADER "=====End_HalfState====="

#define ROL64(a, offset) (((a) << offset) ^ ((a) >> (64-offset)))
#define ROR64(a, offset) (((a) >> offset) ^ ((a) << (64-offset)))
#define ZBIT32(num, z) ((num)>>(z));
#define UINT64 unsigned long long
/*
 * O ile się orientuję, bliżej nas na planie jest najbardziej znaczący bit
 * Nie wiadomo czy zadane wektory we/wy są wstawione jak dokumentacji y^  x> czy może yv x> (naturalna kolejność indexowania)
 *
 *
 */
class InternalState
{
private:
    unsigned long long lanes[5][5];


public:
    static const unsigned long long RC[24];
    static const int rho_offsets[5][5];
    InternalState();
    InternalState set_with_0();
    InternalState set_with_1();
    InternalState set_with_rand();
    InternalState iterate();
    InternalState flat_iterate_l();
    void set_lane(int x, int y, unsigned long long l);
    void xor_lane(int x, int y, unsigned long long l);

    void set_m_significant_by_int_char(const InternalState & ch);

    InternalState theta();
    InternalState pi();
    InternalState rho();
    InternalState chi();
    InternalState iota(int round_num);
    InternalState ptheta();
    InternalState theta3();
    InternalState lambda();
    InternalState keccak_p(int start_on, int number_of_rounds);

    InternalState reverse_theta();
    //InternalState preverse_theta();
    InternalState reverse_pi();
    InternalState reverse_rho();
    InternalState reverse_chi();
    InternalState reverse_iota(int round_num);
    InternalState reverse_lambda();
    InternalState reverse_keccak_p(int start_on, int number_of_rounds);
    InternalState inverse_theta();

    unsigned int get_row(int y, int z) const;
    void set_row(int row, int y, int z);

    unsigned int get_bit(int x, int y, int z) const;
    void set_bit(int x, int y, int z, int v);


    InternalState operator^(const InternalState &s2) const;
    InternalState operator&(const InternalState &s2) const;
    InternalState operator|(const InternalState &s2) const;

    //void reverse_lambda();

    InternalState int_diff() const;
    InternalState diff(const InternalState &) const;


    bool operator==(const InternalState &) const;
    bool operator !=(const InternalState &) const;
    InternalState & operator =(const InternalState);

    bool operator<(const InternalState &) const;


    std::ostream & stream_out_half(std::ostream & os);

    friend std::istream & operator>>(std::istream & is, InternalState &state);
    friend std::ostream & operator<<(std::ostream & os, const InternalState &state);

    void tst();
    static void tst_rhoc();

};




#endif // INTERNALSTATE_H
