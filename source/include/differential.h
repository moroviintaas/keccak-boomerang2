#ifndef DIFFERENTIAL_H
#define DIFFERENTIAL_H
#include "internalstate.h"

class Differential
{
private:
    InternalState input_difference;
    InternalState input_mask;
    InternalState output_difference;
    InternalState output_mask;

public:
    Differential();
    void set_input_difference(const InternalState &s);
    void set_input_mask(const InternalState &s);
    void set_output_difference(const InternalState &s);
    void set_output_mask(const InternalState &s);

    InternalState get_input_difference()const ;
    InternalState get_input_mask()const ;
    InternalState get_output_difference() const ;
    InternalState get_output_mask() const;

    bool is_diff_on_output(const InternalState &s1, const InternalState &s2) const;
    bool is_diff_on_input(const InternalState &s1, const InternalState &s2) const;


};

#endif // DIFFERENTIAL_H
