#include <iostream>
#include <fstream>
#include <internalstate.h>
#include <sstream>
#include "differential.h"
#include <ctime>
#include <vector>
#include "switchdiffs.h"
#include <set>
#include <utility>
#include <bitset>

//using namespace std;

/*void bit_chi(unsigned long long y_array[], int z)
{

}

void bit_reverse_chi(unsigned long long x_array[], unsigned long long result_array[], int z, int y)
{

}*/


void chi_lookup()
{
    InternalState a;
    for (int i=0; i<32; i++)
    {
        std::cout<<std::bitset<5>(i)<<"\t";
        a.set_row(i,0,0);
        a.chi();
        std::cout<<std::bitset<5>(a.get_row(0,0))<<"\n";

    }
}

void load_char_c3(Differential &c3)
{
    std::ifstream ifs;
    InternalState tmp,tmp2,tmp3;
    ifs.open("chars/c3_d.txt",std::ios_base::in);

    ifs>>tmp;
    c3.set_input_difference(tmp);

    tmp.set_with_1();
    c3.set_input_mask(tmp);

    ifs>>tmp;
    ifs>>tmp2;
    //std::cout<<tmp<<tmp2<<"\n\n";
    c3.set_output_difference(tmp2);

    tmp3 = tmp| tmp2;
    tmp2 = tmp ^ tmp2;
    if(tmp3 != tmp2) std::cout<<"Error 1 and 0 at same time\n";
    c3.set_output_mask(tmp3);

    //std::cout<<tmp2<<tmp3<<"\n\n";
    //std::cout<<c3.get_input_difference()<<c3.get_input_mask()<<c3.get_output_difference()<<c3.get_output_mask();
    ifs.close();


}

void load_char_i3(Differential &i3)
{
    std::ifstream ifs;
    InternalState tmp1,tmp2,tmp3;
    ifs.open("chars/i3_d.txt",std::ios_base::in);

    ifs>>tmp1;
    i3.set_input_difference(tmp1);

    ifs>>tmp2;
    i3.set_input_mask(tmp2);
    i3.set_output_mask(tmp2);

    ifs>>tmp3;
    i3.set_output_difference(tmp3);

    //std::cout<<i3.get_input_difference()<<i3.get_input_mask()<<i3.get_output_difference()<<i3.get_output_mask();
    ifs.close();

}
//dedykowane: dla c3 i c4

unsigned int index_to_modify_nbp(unsigned int state_index)
{
    unsigned int result =0;
    for(;state_index>0;state_index--)
        ++result;
    if (result<2) return result;
    else return result+1;
}

void generate_set_of_nabla_prim(std::vector<InternalState> & result, const InternalState &d)
{
    InternalState S1, wS1;
    InternalState wS2,S2 = S1^d;
    InternalState tmp;
    bool contains;
    InternalState v;
    v.set_with_rand();


    for(unsigned int left3=0; left3<16; left3++)
    {
        for(unsigned int right3=0; right3<16; right3++)
        {
            for(unsigned int left4=0; left4<16; left4++)
            {
                for(unsigned int right4=0; right4<16; right4++)
                {
                    //tmp.set_with_0();
                    tmp = v;
                    for(unsigned int k=0; k<4; ++k)
                    {
                        if(((left3>>k)&1)==1)
                            tmp.xor_lane(index_to_modify_nbp(k),3,0x0000800000000000);
                        if(((left4>>k)&1)==1)
                            tmp.xor_lane(index_to_modify_nbp(k),4,0x0000800000000000);
                        if(((right3>>k)&1)==1)
                            tmp.xor_lane(index_to_modify_nbp(k),3,0x0000000000008000);
                        if(((right4>>k)&1)==1)
                            tmp.xor_lane(index_to_modify_nbp(k),4,0x0000000000008000);


                    }
                    S1 = tmp;
                    S2 = S1^d;
                    wS1 = S1;
                    wS2 = S2;
                    wS1.reverse_chi();
                    wS2.reverse_chi();
                    tmp = wS1.diff(wS2);


                    contains = false;
                    for(unsigned int i=0; i<result.size(); i++)
                    {
                        if ( result[i]==tmp) contains = true;
                        break;
                    }
                    if (contains ==false)
                    {
                        //std::cout<<std::dec<<left3<<" "<<left4<<" "<<right3<<" "<<right4<<"\t inserted:\t"<<tmp;

                        result.push_back(tmp);

                        std::cout<<result.size()<<"\n"<<tmp;

                    }
                    else
                    {
                        //std::cout<<std::dec<<left3<<" "<<left4<<" "<<right3<<" "<<right4<<"\t not inserted:\t"<<tmp;
                       // std::cout<<wS1<<wS2<<"\n";
                       // std::cout<<S1<<S2<<"\n\n";
                    }
                }
            }
        }

    }
    //std::cout<<std<<hex<<result.size();


}


void from_nabla_prim_to_diff_before_switch(std::vector<InternalState> & nabla_prims, std::vector<InternalState> &diff_before_switch, int round_num)
{
    InternalState tmp;
    for(unsigned int i=0; i< nabla_prims.size(); i++)
    {

        tmp = nabla_prims[i];
        //std::cout<<"np:\n"<<tmp;
        //tmp.reverse_lambda();
        //std::cout<<tmp.reverse_pi();
        //std::cout<<tmp.reverse_rho();
        InternalState tmp3 = tmp;
        tmp.inverse_theta();

        //tmp.reverse_iota(round_num);
        //std::cout<<"dbs:\n"<<tmp;
        diff_before_switch.push_back(tmp);
        InternalState tmp2 = tmp;
        tmp2 = tmp;
        tmp2.theta();
        //std::cout<<tmp2;
        if(tmp2 != tmp3) std::cout<<"CRITICAL ERROR BAD THETA REVERSE!\n";

    }

}

/*void test(Differential &internal, Differential &classic)
{
    std::set<InternalState> setof_nablaprim;

    InternalState S1;
    InternalState S2 = (S1^classic.get_input_difference());
    InternalState wS1 = S1, wS2 = S2;
    wS1.reverse_chi();
    wS2.reverse_chi();
    //wS1.reverse_lambda();
    //wS2.reverse_lambda();
    std::cout<<wS1<<"\n"<<wS2<<"\n";
    std::cout<<"int diffs: \n"<<wS1.int_diff()<<"\n"<<wS2.int_diff()<<"\n";
    std::cout<<"diff:\n"<<wS1.diff(wS2)<<"\n";
}*/

bool match_row_int(const Differential &internal, const InternalState  &diff_before_switch, unsigned int y, unsigned int z, InternalState & left, InternalState &right, unsigned int starting_l)
{
    unsigned int tmp_row_l, tmp_row_r;
    InternalState lcpy = left, rcpy = right;


    for (unsigned int row = starting_l; row<32; row++)
    {
        left=lcpy;
        left.set_row(row,y,z);
        tmp_row_l = left.get_row(y,z);
        left.set_row(tmp_row_l^internal.get_output_difference().get_row(y,z),y,z+32);
        left.chi();



        for(unsigned int row_r = 0; row_r<32; row_r++)
        {
            right = rcpy;
            right.set_row(row_r,y,z);
            tmp_row_r = right.get_row(y,z);
            right.set_row(tmp_row_r^internal.get_output_difference().get_row(y,z),y,z+32);
            right.chi();


            unsigned int right_low = right.get_row(y,z);
            unsigned int left_low = left.get_row(y,z);
            unsigned int right_high = right.get_row(y,z+32);
            unsigned int left_high = left.get_row(y,z+32);


            unsigned int dl = left.diff(right).get_row(y,z);
            unsigned int dh = left.diff(right).get_row(y,z+32);
            unsigned int dbsl = diff_before_switch.get_row(y,z) ;
            unsigned int dbsh = diff_before_switch.get_row(y,z+32);
            if (( dl== dbsl)&& (dh == dbsh))
            {
                    //std::cout<<"matched on: "<<y<<" "<<z<<"\tthe match is:\n";
                    //std::cout<<left<<right;
                    left.reverse_chi();
                    right.reverse_chi();
                    return true;
            }
        }

    }
    return false;
}

bool match_internal(std::vector<std::pair<InternalState,InternalState>> &result, const Differential &internal, const InternalState  &diff_before_switch, unsigned int limit)
{
    InternalState last, left_iterator;



    std::vector<std::pair<unsigned int, unsigned int>> active_rows;
    std::pair<unsigned int, unsigned int> tmp_pair;
    std::pair<InternalState,InternalState> psh;
    unsigned int y,z;
    for(unsigned int y=0; y<5; y++)
    {
        for (unsigned int z =0; z<32; z++)
        {
            if(diff_before_switch.get_row(y,z)>0 || diff_before_switch.get_row(y,z+32)>0)
            {
                tmp_pair.first=y;
                tmp_pair.second=z;
                active_rows.push_back(tmp_pair);
            }
            else
            {
                //std::cout<<"ia: "<<y<<" "<<z<<"\n";
            }
        }

    }
    //std::cout<<"intdifff:\t\n"<<internal.get_output_difference();
    InternalState left, right;
    for(unsigned int q = 0; q<active_rows.size(); q++)
    {
        y = active_rows[q].first;
        z = active_rows[q].second;

        if(match_row_int(internal,diff_before_switch,y,z,left,right,0))
        {
            /*std::cout<<"left:"<<left;
            std::cout<<"leftint:"<<left.int_diff();
            std::cout<<"right:"<<right;
            std::cout<<"rightint:"<<right.int_diff();
            InternalState tmpl, tmpr;
            tmpl = left; tmpr = right;
            tmpl.chi(); tmpr.chi();
            std::cout<<"afterchileft: "<<tmpl;
            std::cout<<"afterchiright: "<<tmpr;
            std::cout<<tmpl.diff(tmpr);
            */


        }
        else
        {
            std::cout<<"could not match on "<<std::dec<<y<<" "<<z<<"\n";

            //nie udaje się zrobić match'a dla tych y i z
            // trzeba wziąć inny diff_before_switch
            return false;
        }


    }
    std::cout<<"jest szansa, teraz std_diff"<<"\n";
    psh.first = left;
    psh.second = right;
    result.push_back(psh);
    return true;


    /*while(left_iterator<last & result.size()<limit)
    {

    }*/

}


int main()
{
    srand(time(nullptr));
    std::ofstream ofs;
    std::ifstream ifs;

    std::vector<SwitchDiffs> i3c3_switches;

    std::vector<InternalState> nabla_prims;
    std::vector<InternalState> diffs_before_switch;
    std::vector<std::pair<InternalState,InternalState>> matches_on_int;

    diffs_before_switch.clear();


    InternalState state1, state2, state3;
    Differential i3,id4,c3,c4;
    //SwitchDiffs sd1[64];//,sd2[64],sd3[64],sd4[64];

    std::string tmp="", input="";
    unsigned long long ulltest;
   // char c;
    ifs.open("iotest.txt", std::ios_base::in | std::ios_base::binary);

    load_char_c3(c3);
    load_char_i3(i3);

    //ifs>>state1;
    state1.set_with_1();
    state1.set_m_significant_by_int_char(state1);
    state2 = i3.get_output_difference();
    //state2.set_with_rand();
    std::cout<<state1;




    //std::cout<<c3.get_input_difference();
    generate_set_of_nabla_prim(nabla_prims,c3.get_input_difference());
    from_nabla_prim_to_diff_before_switch(nabla_prims,diffs_before_switch,2);
    std::cout<<diffs_before_switch.size();
    std::cout<<diffs_before_switch[0];
    //std::cout<<"\n"<<diffs_before_switch[0].get_row(0,4)<<"\n"<<diffs_before_switch[0].get_bit(3,0,4)<<"\n";
    std::cout<<std::bitset<5>(diffs_before_switch[0].get_row(0,1))<<"\t"<<std::bitset<5>(diffs_before_switch[0].get_row(0,33))<<"\t"<<std::bitset<5>(i3.get_output_difference().get_row(0,1))<<"\n\n";
    /*for (unsigned int i=0; i< diffs_before_switch.size(); ++i)
    {
        std::cout<<"diff: "<<i<<"\n";
        match_internal(matches_on_int,i3,diffs_before_switch[i],10);
    }*/

    std::cout<<nabla_prims[0]<<"\n";
    //ifs>>state1;
    state1 = nabla_prims[0];
    std::cout<<"1: "<<state1<<"\n";
   //std::cout<<"2: "<<state1.pi()<<"\n";
   //std::cout<<"1: "<<state1.rho()<<"\n";
   //std::cout<<"2: "<<state1.theta()<<"\n";
    //std::cout<<"2: "<<state1.lambda()<<"\n";
    //chi_lookup();
    //match_internal(matches_on_int,i3,diffs_before_switch[0],10);




    //test(i3,c3);





    return 0;

}
