#include "switchdiffs.h"

SwitchDiffs::SwitchDiffs()
{

}

SwitchDiffs::SwitchDiffs(const InternalState &L, const Differential &diff, int iota_num)
{
    Left = L;
    Right = Left ^ diff.get_input_difference();
    L_before_chi_std = Left;
    L_before_chi_std.reverse_chi();
    R_before_chi_std = Right;
    R_before_chi_std.reverse_chi();

    L_after_chi_int=L_before_chi_std;
    L_after_chi_int.reverse_lambda();
    L_after_chi_int.reverse_iota(iota_num);

    R_after_chi_int=R_before_chi_std;
    R_after_chi_int.reverse_lambda();
    R_after_chi_int.reverse_iota(iota_num);
}

InternalState SwitchDiffs::getLeft() const
{
    return Left;
}


InternalState SwitchDiffs::getRight() const
{
    return Right;
}


void SwitchDiffs::set(const InternalState &value, const Differential &diff, int iota_num)
{
    Left = value;
    Right = Left ^ diff.get_input_difference();
    L_before_chi_std = Left;
    L_before_chi_std.reverse_chi();
    R_before_chi_std = Right;
    R_before_chi_std.reverse_chi();

    L_after_chi_int=L_before_chi_std;
    L_after_chi_int.reverse_lambda();
    L_after_chi_int.reverse_iota(iota_num);

    R_after_chi_int=R_before_chi_std;
    R_after_chi_int.reverse_lambda();
    R_after_chi_int.reverse_iota(iota_num);
}

InternalState SwitchDiffs::getL_after_chi_int() const
{
    return L_after_chi_int;
}

InternalState SwitchDiffs::getR_after_chi_int() const
{
    return R_after_chi_int;
}

InternalState SwitchDiffs::getL_before_chi_std() const
{
    return L_before_chi_std;
}

InternalState SwitchDiffs::getR_before_chi_std() const
{
    return R_before_chi_std;
}
