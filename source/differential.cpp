#include "differential.h"
#include "internalstate.h"
Differential::Differential()
{

}

void Differential::set_input_difference(const InternalState &s)
{
    input_difference = s;
}

void Differential::set_input_mask(const InternalState &s)
{
    input_mask = s;
}

void Differential::set_output_difference(const InternalState &s)
{
    output_difference = s;
}

void Differential::set_output_mask(const InternalState &s)
{
    output_mask=s;
}

InternalState Differential::get_input_difference() const
{
    return input_difference;
}

InternalState Differential::get_input_mask() const
{
    return input_mask;
}

InternalState Differential::get_output_difference() const
{
    return output_difference;
}

InternalState Differential::get_output_mask() const
{
    return output_mask;
}

bool Differential::is_diff_on_output(const InternalState &s1, const InternalState &s2) const
{
    InternalState x = s1^s2;
    x = (x & output_mask);
    if (x == (output_difference&output_mask))
        return true;
    else return false;
}

bool Differential::is_diff_on_input(const InternalState &s1, const InternalState &s2) const
{
    InternalState x = s1^s2;
    x = (x & input_mask);
    if (x == (input_difference&input_mask))
        return true;
    else return false;
}
